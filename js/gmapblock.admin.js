(function($) {

  Drupal.behaviors.gmapblock = {
    attach : function(context, settings) {

      var admin_initialize = function() {
        var map = new google.maps.Map(document.getElementById("map-canvas")),
            markers = [],
            marker = new google.maps.Marker({
              draggable : true
            }),
            mapOptions = {
              center : new google.maps.LatLng(44.3524508349238, 381.1646298828125),
              panControl : false,
              streetViewControl : false,
              zoom : 7,
              zoomControl : true,
              zoomControlOptions : {
                style : google.maps.ZoomControlStyle.SMALL
              },
              mapTypeId : google.maps.MapTypeId.ROADMAP,
              mapTypeControlOptions : {
                style : google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                mapTypeIds : gmapblock.mapTypeIds().concat([ google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE ])
              }
            };

        $mapZoom = $('input[name="settings[zoom]"]');
        $mapMarker = $('input[name="settings[marker]"]');
        $mapCenter = $('input[name="settings[center]"]');
        $mapType = $('input[name="settings[type]"]');
        $mapAspect = $('select[name="settings[mapaspect][aspect]"]');
        $mapContainer = $('select[name="settings[mapaspect][container-size]"]');

        // Set zoom
        if ($mapZoom.val()) {
          mapOptions.zoom = parseInt($mapZoom.val());
        }
        // Set center
        var latlng = $mapCenter.val().split(',');
        if (latlng.length == 2) {
          mapOptions.center = new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1]));
        }
        // Set map marker
        latlng = $mapMarker.val().split(',');
        if (latlng.length == 2) {
          marker.setPosition(new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1])));
          marker.setMap(map);
        }
        // Set map type
        if ($mapType.val()) {
          mapOptions.mapTypeId = $mapType.val();
        }

        map.setOptions(mapOptions);
        gmapblock.setMapStyles(map);

        // Google Maps Bug fix
        $('.fieldset-wrapper').bind('beforeShow', function() {
          var id = $(this).closest('fieldset')[0].id;
          if (id == 'gmapblock-settings') {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(mapOptions.center);
          }
        });

        $.each(['show'], function(i, ev) {
          var el = $.fn[ev];
          $.fn[ev] = function() {
            this.trigger(ev);
            this.trigger('beforeShow');
            return el.apply(this, arguments);
          };
        });

        google.maps.event.addListenerOnce(map, 'idle', function() {
          google.maps.event.trigger(map, 'resize');
        });

        google.maps.event.addListener(map, 'click', function(e) {
          marker.setMap(map);
          marker.setPosition(e.latLng);

          $mapMarker.val(marker.getPosition().toUrlValue());
          map.panTo(e.latLng);
        });

        google.maps.event.addListener(marker, 'dragend', function(e) {
          console.log(marker.getPosition().toUrlValue());
          $mapMarker.val(marker.getPosition().toUrlValue());
        });

        // Delete marker on click
        // google.maps.event.addListener(marker, 'click', function(e) {
        // marker.setMap(null);
        // $mapMarker.val('');
        // });

        google.maps.event.addListener(map, 'zoom_changed', function() {
          $mapZoom.val(map.getZoom());
        });

        google.maps.event.addListener(map, 'center_changed', function() {
          $mapCenter.val(map.getCenter().toUrlValue());
        });

        google.maps.event.addListener(map, 'maptypeid_changed', function() {
          console.log(map.getMapTypeId());
          $mapType.val(map.getMapTypeId());
        });

        $mapAspect.change(function() {
          $('#map-canvas').css({
            width : '100%',
            height : '100%',
            paddingBottom : this.value
          });
          google.maps.event.trigger(map, 'resize');
          map.setCenter(mapOptions.center);
        });

        $mapContainer.change(function() {
          switch (this.value) {
            case 'wide':
              width = '100%';
              height = '100%';
              break;

            case 'small':
              width = '30%';
              height = '50%';
              break;

            case 'normal':
            default:
              width = '50%';
              height = '70%';
              break;
          }
          $('#map-container').css({
            width : width,
            height : height,
          });

          $mapAspect.trigger('change');

          //google.maps.event.trigger(map, 'resize');
          //map.setCenter(mapOptions.center);
        }).trigger('change');

      };

      google.maps.event.addDomListener(window, "load", admin_initialize);
    }
  };
  
})(jQuery);