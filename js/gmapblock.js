(function($) {

  Drupal.behaviors.gmapblock = {
    attach : function(context, settings) {

      var initialize = function() {

        var mapSettings = settings.gmapblock,
            map = new google.maps.Map(document.getElementById("map-canvas")),
            mapTypes = [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE ],
            marker = new google.maps.Marker({
              position : new google.maps.LatLng(mapSettings.marker.lat, mapSettings.marker.lng),
              map : map,
            });

        if (mapSettings.type && gmapblock.mapStyles[mapSettings.type]) {
          var customMapType = new google.maps.StyledMapType(
              gmapblock.mapStyles[mapSettings.type], {
                name : mapSettings.type
              });

          map.mapTypes.set(mapSettings.type, customMapType);
          mapTypes.push(mapSettings.type);
          map.setMapTypeId(mapSettings.type);
        }

        var mapOptions = {
          center : new google.maps.LatLng(mapSettings.center.lat, mapSettings.center.lng),
          zoom : mapSettings.zoom,
          zoomControl : true,
          zoomControlOptions : {
            style : google.maps.ZoomControlStyle.SMALL
          },
          panControl : false,
          streetViewControl : false,
          // mapTypeId: google.maps.MapTypeId.ROADMAP,
          mapTypeControlOptions : {
            style : google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            mapTypeIds : mapTypes
          }
        };

        map.setOptions(mapOptions);

        if (mapSettings.show_body && (mapSettings.body && mapSettings.body.value != '')) {
          var infowindow = new google.maps.InfoWindow({
            content : mapSettings.body.value,
          });
          google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
          });
        }
        
        google.maps.event.addListenerOnce(map, 'idle', function(){
          google.maps.event.trigger(map, 'resize');
          map.setCenter(mapOptions.center);
        });
        
        $(window).bind('resize', function() {
          google.maps.event.trigger(map, 'resize');
          map.setCenter(mapOptions.center);
        });
      };

      google.maps.visualRefresh = true;
      google.maps.event.addDomListener(window, "load", initialize);

    }
  };

})(jQuery);